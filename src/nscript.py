import re
import os
import sys
import shutil


class scene:

    def __init__(self, script, no):
        self.script = script
        self.no = no

    def getConversation(self):
        Conversation = re.findall("【(.*?)】\n(.*?)\\\\", self.script)
        return Conversation

    def output(self, nsa, output):
        bgms = re.findall('bgm "(.*?)"', self.script)
        bgs = re.findall('bg "(.*?)"', self.script)
        lds = re.findall('ld .*?".*?;(.*?)"', self.script)
        dwaves = re.findall('dwave \d+?,"(.*?)"', self.script)
        print('导出普通场景%s | 背景音乐:%d | 背景:%d | 人物:%d | 语音:%d' % (
            self.no, len(bgms), len(bgs), len(lds), len(dwaves)))
        os.mkdir('{}\\scene{}'.format(output, self.no))

        if bgms != []:
            os.mkdir('{}\\scene{}\\bgm'.format(output, self.no))
            for bgm in bgms:
                shutil.copy('{}\\{}'.format(nsa, bgm),'{}\\scene{}\\bgm\\{}'.format(output, self.no,re.search('/(.*?)', bgm).group(1)))

        if bgs != []:
            os.mkdir('{}\\scene{}\\bg'.format(output, self.no))
            for bg in bgs:
                shutil.copy('{}\\{}'.format(nsa,bg),'{}\\scene{}\\bg\\{}'.format(output, self.no, re.search('/(.*?)',bg).group(1)))

        if lds != []:
            os.mkdir('{}\\scene{}\\ld'.format(output, self.no))
            for ld in lds:
                shutil.copy('{}\\{}'.format(nsa,ld),'{}\\scene{}\\ld\\{}'.format(output, self.no, re.search('/(.*?)',ld).group(1)))

        if dwaves != []:
            os.mkdir('{}\\scene{}\\voice'.format(output, self.no))
            for dwave in dwaves:
                shutil.copy('{}\\{}'.format(nsa,dwave),'{}\\scene{}\\voice\\{}'.format(output, self.no, re.search('/(.*?)',dwave).group(1)))

        with open('{}\\scene{}\\nscript.txt'.format(output,self.no), 'w') as file:
            file.write(self.script)


class hscene:

    def __init__(self, script, no):
        self.script = script
        self.no = no

    def getConversation(self):
        Conversation = re.findall("【(.*?)】\n(.*?)\\\\", self.script)
        return Conversation

    def output(self, nsa, output):
        bgms = re.findall('bgm "(.*?)"', self.script)
        bgs = re.findall('bg "(.*?)"', self.script)
        lds = re.findall('ld .*?".*?;(.*?)"', self.script)
        dwaves = re.findall('dwave \d+?,"(.*?)"', self.script)
        print('导出H场景%s | 背景音乐:%d | 背景:%d | 人物:%d | 语音:%d' % (self.no,len(bgms),len(bgs),len(lds),len(dwaves)))
        os.mkdir('{}\\hscene{}'.format(output, self.no))

        if bgms != []:
            os.mkdir('{}\\hscene{}\\bgm'.format(output, self.no))
            for bgm in bgms:
                shutil.copy('{}\\{}'.format(nsa,bgm),'{}\\hscene{}\\bgm\\{}'.format(output, self.no, re.search('/(.*?)',bgm).group(1)))

        if bgs != []:
            os.mkdir('{}\\hscene{}\\bg'.format(output, self.no))
            for bg in bgs:
                shutil.copy('{}\\{}'.format(nsa,bg),'{}\\hscene{}\\bg\\{}'.format(output, self.no, re.search('/(.*?)',bg).group(1)))

        if lds != []:
            os.mkdir('{}\\hscene{}\\ld'.format(output, self.no))
            for ld in lds:
                shutil.copy('{}\\{}'.format(nsa,ld),'{}\\hscene{}\\ld\\{}'.format(output, self.no, re.search('/(.*?)',ld).group(1)))

        if dwaves != []:
            os.mkdir('{}\\hscene{}\\dwave'.format(output, self.no))
            for dwave in dwaves:
                shutil.copy('{}\\{}'.format(nsa,dwave),'{}\\hscene{}\\dwave\\{}'.format(output, self.no, re.search('/(.*?)',dwave).group(1)))

        with open('{}\\hscene{}\\nscript.txt'.format(output,self.no), 'w') as file:
            file.write(self.script)


class Nscript:

    def __init__(self, file):
        os.system('title ONScript Interpreter')
        file = self.crage(file)
        with open(file, "r") as file:
            self.script = file.read()
        self.title = re.search('caption "(.*?)"', self.script).group(1)
        self.scene = re.findall('\n\*scene(\d+)', self.script)
        self.hscene = re.findall('\n\*hscene(\d+)', self.script)
        print(self.title)
        print('普通场景数:', len(self.scene))
        print('H场景数:', len(self.hscene))

    def crage(self, nscript, output=None):
        if output is None:
            command = '{}\\crage.exe -p {}'.format(sys.path[0], nscript)
            output = 'nscript.txt'
        else:
            command = '{}\\crage.exe -p {} -o {}'.format(sys.path[0], nscript, output)
        os.system(command)
        os.system('cls')
        return output

    def getScene(self, no):
        if len(no) > len(self.scene):
            raise IndexError('Scene %d no found' % no)
        if len(str(no)) == 1:
            no = '0' + str(no)
        else:
            no = str(no)
        return scene(re.search('\n(\*scene%s\n.*?\nend)' % no, self.script, re.S).group(1),no)

    def getHScene(self, no):
        if len(no) > len(self.hscene):
            raise IndexError('HScene %d no found' % no)
        if len(str(no)) == 1:
            no = '0' + str(no)
        else:
            no = str(no)
        return hscene(re.search('\n(\*hscene%s\n.*?\nend)' % no, self.script, re.S).group(1),no)


Nscript = Nscript('D:\\crass-0.4.14.0\\该死的妹子\\nscript.dat')
for scen in Nscript.scene:
    sc = Nscript.getScene(scen)
    sc.output('D:\\crass-0.4.14.0\\该死的妹子\\arc','D:\\crass-0.4.14.0\\该死的妹子\\output')
for hscen in Nscript.hscene:
    sc = Nscript.getHScene(hscen)
    sc.output('D:\\crass-0.4.14.0\\该死的妹子\\arc','D:\\crass-0.4.14.0\\该死的妹子\\output')
